package com.example.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name ="errorhandler")
@XmlRootElement

public class BackendErrorMapping implements Serializable {
	@Override
	public String toString() {
		return "BackendErrorMapping [id=" + id + ", httpStatus=" + httpStatus + ", description=" + description
				+ ", groupId=" + groupId + "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int getId() {
		return id;
	}
    
	public void setId(int id) {
		this.id = id;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	@JsonProperty
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@JsonProperty
	@Column(name = "httpstatus")
	@Min(200)
	private int httpStatus;
	@JsonProperty
	@Column(name = "description")
	@NotNull
	private String description;
	@JsonProperty
	@Column(name = "exception")
	@NotNull
	private String groupId;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
