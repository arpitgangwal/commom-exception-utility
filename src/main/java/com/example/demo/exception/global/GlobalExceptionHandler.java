package com.example.demo.exception.global;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.example.demo.dao.IBackendError;
import com.example.demo.entity.BackendErrorMapping;

@ControllerAdvice

public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	@Autowired
	private IBackendError backendDao;

	@ExceptionHandler(SQLException.class)
	public String handleSQLException(HttpServletRequest request, Exception ex) {
		logger.info("SQLException Occured:: URL=" + request.getRequestURL());
		return "database_error";
	}
	//
	// @ExceptionHandler({
	// org.springframework.security.access.AccessDeniedException.class })
	// public ResponseEntity<String> handleAccessDeniedException(Exception ex,
	// WebRequest request) {
	// return new ResponseEntity<>(
	// "Access denied message here", HttpStatus.FORBIDDEN);
	// }

	@ExceptionHandler({ResourceNotFoundException.class, NoHandlerFoundException.class})
    private ResponseEntity<Void> handleResourceNotFoundException(ResourceNotFoundException e) {
		logger.error("IOException handler executed");
System.out.println("Error occurred");
        return ResponseEntity.notFound().build();
    }

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<String> handleBadInputException(Exception e) {
		logger.error("IOException handler executed");
		return new ResponseEntity<>(e.getMessage(), HttpStatus.METHOD_NOT_ALLOWED);
	}

	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<String> handleConflict(RuntimeException ex, WebRequest request) {
		String bodyOfResponse = "This should be application specific";
		return new ResponseEntity<>(bodyOfResponse, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(ResourceAccessException.class)
	protected ResponseEntity<String> handleBusinessException(ResponseError responseError) {
		logger.error("IOException handler executed");
		// returning 500 error code
		return new ResponseEntity<>(responseError.getBody(), responseError.getStatusCode());
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "IOException occured")
	@ExceptionHandler(Exception.class)
	public ResponseEntity<List<BackendErrorMapping>> handleGeneralException(Exception e) {
		logger.error(e.getMessage(), e.getClass());
		List<BackendErrorMapping> list = backendDao.getDescription(e.getClass().getName().toString());
		return new ResponseEntity<>(list, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
