package com.example.demo.exception.global;

import java.io.IOException;
import java.nio.charset.Charset;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.ResponseErrorHandler;

public class GlobalErrorHandler implements ResponseErrorHandler {

    
    /**
	 * Delegates to {@link #hasError(HttpStatus)} with the response status code.
	 */
	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		HttpStatus statusCode = response.getStatusCode();
		return (statusCode != null && hasError(statusCode));
	}

	/**
	 * Template method called from {@link #hasError(ClientHttpResponse)}.
	 * <p>The default implementation checks if the given status code is
	 * {@link HttpStatus.Series#CLIENT_ERROR CLIENT_ERROR} or
	 * {@link HttpStatus.Series#SERVER_ERROR SERVER_ERROR}.
	 * Can be overridden in subclasses.
	 * @param statusCode the HTTP status code
	 * @return {@code true} if the response has an error; {@code false} otherwise
	 */
	protected boolean hasError(HttpStatus statusCode) {
		return !(HttpStatus.Series.SUCCESSFUL == statusCode.series());
	}
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
    	
    	String body =read(response);
    	ResponseError exception = new ResponseError(response.getStatusCode(), body, response.getStatusText());
        throw exception;
    }
    public static String read(ClientHttpResponse response) throws IOException {
    	String bodyText = StreamUtils.copyToString(response.getBody(), Charset.defaultCharset());
       return bodyText;
    }

}
