package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IBackendError;
import com.example.demo.entity.BackendErrorMapping;
@Service
public class ErrorService implements IErrorService{
  @Autowired IBackendError backendErrorDAO;
	@Override
	public boolean insertError(BackendErrorMapping b) {
		// TODO Auto-generated method stub
		if(backendErrorDAO.alreadyErrorMappingPresent(b))
		return false;
		else{
			backendErrorDAO.insertError(b);
			return true;
		}
	}

	@Override
	public BackendErrorMapping getError(int id) {
		// TODO Auto-generated method stub
		return backendErrorDAO.getError(id);
	}

}
