package com.example.demo.service;

import com.example.demo.entity.BackendErrorMapping;

public interface IErrorService {
	
	boolean insertError(BackendErrorMapping b);
	BackendErrorMapping getError(int id);

}
