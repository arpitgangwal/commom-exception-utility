package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;

public class PropertyBuilder {
	@Value( "${logging.level}" )
	private String level;

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
