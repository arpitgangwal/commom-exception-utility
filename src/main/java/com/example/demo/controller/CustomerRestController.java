package com.example.demo.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entity.BackendErrorMapping;
import com.example.demo.exception.global.GlobalErrorHandler;
import com.example.demo.service.IErrorService;

/**
 * This is rest controller class for handling all the customer requests.
 *
 */
@RestController
@RequestMapping("/error")
public class CustomerRestController {
	
	/** Create LoggerFactory for UpdateAddressCommandHandler. */
	private static final Logger LOG = LoggerFactory.getLogger(CustomerRestController.class);
    @Autowired IErrorService errorService;
	@PostMapping(value ="/addError" , consumes="application/json")
	public ResponseEntity<Void> addError(@Valid @RequestBody BackendErrorMapping backendErrorMapping) {
                boolean flag = errorService.insertError(backendErrorMapping);
                if (flag == false) {
                    LOG.debug("Error Object is duplicate");
        	    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
                }
                LOG.debug("Error Object successfully added");
                return new ResponseEntity<Void>( HttpStatus.CREATED);
	}

	@GetMapping(value ="/getError/{errorId}" ,  produces="application/json")
	public ResponseEntity<BackendErrorMapping> getError(@PathVariable("errorId") Integer id) {
		BackendErrorMapping backendErrorMapping = errorService.getError(id);
                if (backendErrorMapping == null) {
                    LOG.debug("Error Object is duplicate");

        	    return new ResponseEntity<BackendErrorMapping>(HttpStatus.NO_CONTENT);
                }
                LOG.debug("Error Object successfully added");
                return new ResponseEntity<BackendErrorMapping>(backendErrorMapping, HttpStatus.OK);
	}
	//Demo for the Error Handler from source
	@GetMapping(value ="/getErrorConsumer/{id}")
	public ResponseEntity<String> getErrorSource(@PathVariable("id") String id) {
		RestTemplate rt = new RestTemplate();
		HashMap<String,String> uriVariables= new HashMap<String,String>();
		uriVariables.put("id", id);
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        rt.setErrorHandler(new GlobalErrorHandler());
        rt.getForEntity("http://127.0.0.1:8081/error/addError/{id}", String.class, uriVariables);
		return new ResponseEntity<>("Success",HttpStatus.ACCEPTED);
	}
}
