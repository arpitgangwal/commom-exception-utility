package com.example.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.BackendErrorMapping;

@Transactional
@Repository
public class BackendErrorDAO implements IBackendError {
	@PersistenceContext	
	private EntityManager entityManager;	
	@Override
	public void insertError(BackendErrorMapping b) {

           entityManager.persist(b);
	}

	@Override
	public BackendErrorMapping getError(int id) {
		// TODO Auto-generated method stub
		return entityManager.find(BackendErrorMapping.class, id);
	}

	@Override
	public boolean alreadyErrorMappingPresent(BackendErrorMapping b) {
		// TODO Auto-generated method stub
		String hql = "FROM BackendErrorMapping as er WHERE er.description = ?";
		int count = entityManager.createQuery(hql).setParameter(1, b.getDescription())
		              .getResultList().size();
		return count > 0 ? true : false;	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BackendErrorMapping> getDescription(String groupId) {
		String hql = "FROM BackendErrorMapping as er WHERE er.groupId like :groupId";
		return entityManager.createQuery(hql).setParameter("groupId", "%"+groupId+"%")
		              .getResultList();
	
	}

}
