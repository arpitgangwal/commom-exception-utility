package com.example.demo.dao;

import java.util.List;

import com.example.demo.entity.BackendErrorMapping;

public interface IBackendError {

	void insertError(BackendErrorMapping b);
	BackendErrorMapping getError(int id);
	boolean alreadyErrorMappingPresent(BackendErrorMapping b);
	List<BackendErrorMapping> getDescription(String groupId);
}
